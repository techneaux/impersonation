﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Impersonation
{
    class Program
    {
        
        
        static void Main(string[] args)
        {
            
                    System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\\AlarmTest.txt", true);

            file.WriteLine("Inside Main");
            //args[2] is GNS or CAS
            if (args[1] != "CAS")
            {
                if (args[2] == "GNS")
                {
                    //args[0] = fileName args[1] = ackID args[3] = gnsName args[4] = desc args[5] =facDesc;

                    AckGNS(args[1], args[3], args[0], args[4], args[5], args[6], args[7]);

                    return;
                }
            }


            System.Security.Principal.WindowsIdentity currentIdentity = System.Security.Principal.WindowsIdentity.GetCurrent();

            
            file.WriteLine(args[0]);
            string tag = args[0];
            string[] tagList;
            tagList = tag.Split(',');

            foreach (var oneTag in tagList)
            {
                string[] tokenarray;
                tokenarray = oneTag.Split('.');
                string siteService = tokenarray[0] + "." + tokenarray[1];

                CXSCRIPTLib.GlobalFunctions cxGlobalFunctions = new CXSCRIPTLib.GlobalFunctions();
                string casService = cxGlobalFunctions.GetAssociatedServiceName(siteService, "CAS");
                CxCasLib.CasClient cxCasClient = new CxCasLib.CasClient();

                cxCasClient.Connect(casService);

                cxGlobalFunctions.EnableLiveMode(true);

                string strUserID = cxCasClient.UserId;

                CXSCRIPTLib.Points cxPoints = new CXSCRIPTLib.Points();

                string[] tokenTooArray = tokenarray[2].Split(':');
                string pointTag = tokenarray[0] + "." + tokenarray[1] + ":" + tokenTooArray[1];
                string ackTag = tokenarray[0] + "." + tokenarray[1] + "." + tokenTooArray[0];
                string[] facID = tokenTooArray[1].Split('_');

                cxPoints.AddPoint(pointTag + ";" + "ack");

                cxPoints.UpdateNow(0);

                CXSCRIPTLib.Point myPoint = cxPoints.Point(pointTag);

                string alarmVersion = myPoint.GetAttribute("alarmrecversion");

                bool bnCanAckAlarm = cxCasClient.CanUserDoAlarmAction("ACK", ackTag, strUserID);

                string desc = GetPointDescription(oneTag);
                string facDesc = GetFacilityDescription(tokenarray[0], tokenarray[1], facID[0]);
                file.WriteLine(strUserID + " " + bnCanAckAlarm);
                if (bnCanAckAlarm)
                {
                    try
                    {
                        file.WriteLine("Alarm was Acknowledged");
                        cxCasClient.DoAlarmAction("ACK", ackTag, alarmVersion, "This alarm was acknowledged by " + strUserID);
                        file.Close();
                        cxCasClient.Disconnect();
                    }
                    catch (Exception ex)
                    {
                        file.WriteLine("Exception" + ex.Message);
                        file.Close();
                        cxCasClient.Disconnect();
                        throw new Exception("Acknowledmgent Error: " + ex.Message);
                    }
                }
            }
            return;
        }

        public static string GetPointDescription(string tag)
        {
            CXSCRIPTLib.GlobalFunctions globalfuncs = new CXSCRIPTLib.GlobalFunctions();
            CXSCRIPTLib.Points cxPoints = new CXSCRIPTLib.Points();
            CXSCRIPTLib.Point cxPoint = cxPoints.Point(tag);
            string description = cxPoint.Description;
            return description;
        }

        public static string GetFacilityDescription(string siteName, string service, string facid)
        {
            CXSCRIPTLib.GlobalFunctions gFuncs = new CXSCRIPTLib.GlobalFunctions();

            string facService = gFuncs.GetAssociatedFacName(siteName + "." + service);
            CxFacLib.FacClient cxFacClient = new CxFacLib.FacClient();
            cxFacClient.Connect(facService);
            CXSCRIPTLib.Facilities cxFac = new CXSCRIPTLib.Facilities();
            var facTag = siteName + "." + service + "::" + facid;
            string description = cxFac.GetFacilityAttribute(facTag, "facility_desc");
            return description;
        }

        static void AckGNS(string ackID, string gnsName, string fileName, string pointDesc, string facDesc, string casName, string ackTag)
        {
            System.IO.StreamWriter gnsFile = new System.IO.StreamWriter(fileName, true);
            CxGnsLib.GnsClient cxGNS = new CxGnsLib.GnsClient();
            cxGNS.Connect(gnsName);
            CxCasLib.CasClient cxCasClient = new CxCasLib.CasClient();

            cxCasClient.Connect(casName);
            string strUserID = cxCasClient.UserId;

            bool res;
            string[] ackList = ackID.Split(',');
            string[] facDescList = facDesc.Split(',');
            string[] descList = pointDesc.Split(',');
            string[] ackTagList = ackTag.Split(',');
           
            object resend;
            cxGNS.GetResendQueueEntries(out resend);
            object[,] resendArr = (Object[,])resend;

            for(int i = 0; i < ackList.Length; i++ )
            {
                try
                {
                    if (cxCasClient.CanUserDoAlarmAction("ACK", ackTagList[i], strUserID))
                    {
                        res = cxGNS.AcknowledgeNotification(ackList[i]);
                        if (res)
                        {
                            gnsFile.WriteLine(facDescList[i] + " " + descList[i] + " Ack ID: " + ackList[i] + " Notification was acknowledged.");
                        }
                        else
                        {
                            gnsFile.WriteLine(facDescList[i] + " " + descList[i] + " Ack ID: " + ackList[i] + " Notification was NOT acknowledged.");
                        }
                    }
                    else
                    {
                        gnsFile.WriteLine("Access Denied.");
                    }
                }
                catch (Exception ex)
                {
                    gnsFile.WriteLine("Notification Acknowledgment Error: " + ex.Message);
                    gnsFile.Close();
                }
            }
            cxGNS.Disconnect();
            gnsFile.Close();
            return;
        }
    }
}

